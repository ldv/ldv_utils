"""
This module provides a REST interface to the LDV specifications

Could be that this should be replaced with ldv_specification_interface module/python package
Its kinda prototype module for now required for the migration script
"""
import requests
import logging

from json.decoder import JSONDecodeError

try:
    from simplejson.errors import JSONDecodeError as SimpleJSONDecodeError
except ModuleNotFoundError:
    from json.decoder import JSONDecodeError as SimpleJSONDecodeError

from urllib.parse import urlparse, urlunparse

# ==============================================================
# The request header
REQUEST_HEADER = {
    'content-type': "application/json",
    'cache-control': "no-cache"
}
LDV_HOST_DEV = "http://localhost:8000/ldvspec/api/v1"  # your local development environment with Django webserver
LDV_HOST_TEST = "https://sdc-dev.astron.nl/ldvspec/api/v1"  # the ldv sdc test environment.
LDV_HOST_PROD = "https://sdc.astron.nl/ldvspec/api/v1"  # the ldv sdc production environment.


class APIException(Exception):
    pass


class APIMissing(Exception):
    pass


def is_http_exception(response: requests.Response):
    if response.status_code in range(400, 499):
        return True
    return False


def is_success(response: requests.Response):
    if response.status_code in range(200, 300):
        return True
    return False


def is_missing(response: requests.Response):
    if response.status_code == 404:
        return True
    return False


def can_retry(response: requests.Response):
    if response.status_code in [408, 429]:
        return True
    return False


class LDVSpecInterface:
    """
    This class is used to connect via REST interface
    """

    def __init__(self, host, token):
        """
        Constructor.
        :param host: the host name of the backend.
        :param token: The token to login
        """
        # accept some presets to set host to dev, test, acc or prod
        self.host = host
        if self.host == 'dev':
            self.host = LDV_HOST_DEV
        elif self.host == 'test':
            self.host = LDV_HOST_TEST
        elif self.host == 'prod':
            self.host = LDV_HOST_PROD
        if not self.host.endswith('/'):
            self.host += '/'
        self.header = REQUEST_HEADER
        self.header['Authorization'] = f'Token {token}'
        self._session = None

    def session(self):
        if self._session is None:
            self._session = requests.Session()
            self._session.headers.update(self.header)
        return self._session

    def _request(self, url, type, query_parameters=None, payload=None):
        parsed_url = urlparse(url)
        if not parsed_url.path.endswith('/'):
            parsed_url = parsed_url._replace(path=parsed_url.path + '/')

        url = urlunparse(parsed_url)

        if isinstance(payload, str):
            response = self.session().request(type, url, data=payload, headers=self.header, params=query_parameters)
        else:
            response = self.session().request(type, url, json=payload, headers=self.header, params=query_parameters)

        logging.debug(f"[{type} {response.url} ]")
        logging.debug("Response: " + str(response.status_code) + ", " + str(response.reason))
        if is_missing(response):
            raise APIMissing(url)
        elif is_http_exception(response) and can_retry(response):
            return self._request(url, type, query_parameters, payload)
        elif is_http_exception(response):
            raise APIException(f'{response.status_code}: {response.reason} {response.content}')
        elif is_success(response):
            try:
                json_response = response.json()

                return json_response
            except (SimpleJSONDecodeError, JSONDecodeError):
                return response.content
        raise APIException(f'Unrecognized response code {response.status_code}: {response.reason} {response.content}')
        # raise (Exception("ERROR: " + response.url + " not found."))

    # === Backend requests ================================================================================
    def do_POST_json(self, resource, payload):
        """
        POST a payload to a resource (table). This creates a new object (observation or dataproduct)
        This function replaces the old do_POST function that still needed to convert the json content in a very ugly
        :param resource: contains the resource, for example 'observations', 'dataproducts'
        :param payload: the contents of the object to create in json format
        """
        url = self.host + resource
        if not resource.endswith('/'):
            resource += '/'
        logging.debug(f'do_POST_json using url={url} and with payload: {payload}')
        try:
            json_response = self._request(url, 'POST', payload=payload)
            if hasattr(json_response, 'id'):
                return json_response['id']
            else:
                return -1
        except Exception as err:
            raise err

    def insert_multiple_dataproduct(self, payload):
        """
        Insert multiple dataproducts. Implicit also dataproduct-location is added
        :param: List of payload string
        :return: List id of added dataproducts
        """
        url = self.host + "insert_dataproduct/"
        logging.debug(f'insert_multiple_dataproduct using url={url} and with payload: {payload}')
        try:
            json_response = self._request(url, 'POST', payload=payload)
            response_lst_ids = []
            for resp in json_response:
                response_lst_ids.append(resp['id'])
            return response_lst_ids
        except Exception as err:
            raise err
