#!/usr/bin/env python3
"""
This script migrates data from ldvadmin to ldv-spec-db with user arguments.
By default, the local ldv-spec-db is used and the ldvadmin on sdc-db.astron.nl
Script requires token to access the ldv-spec-db with REST API

Some examples:
- Show latest version:
     python ./ldvspec/lofardata/scripts/migrate_ldvadmin_to_ldvspec.py --version
- Import only 1000 records and show more verbosity:
      python ./ldvspec/lofardata/scripts/migrate_ldvadmin_to_ldvspec.py --limit 1000 --verbose
- Import 50000 records and insert in steps of 10000 (so 5 steps)
      python ./ldvspec/lofardata/scripts/migrate_ldvadmin_to_ldvspec.py --limit 50000 --max_nbr_dps_to_insert_per_request 10000
- Import only 1000 records at production:
      python ./ldvspec/lofardata/scripts/migrate_ldvadmin_to_ldvspec.py --limit 1000 --host prod

"""
import argparse
import logging
import math
import os
import sys
import time

import ldv_migrate.connection.retrieve_db_connection as connector
from ldv_migrate.ldv_specification_interface import LDVSpecInterface

logger = logging.getLogger(__file__)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)


def change_logging_level(level):
    logger = logging.getLogger()
    logger.setLevel(level)
    for handler in logger.handlers:
        handler.setLevel(level)


def execute_query(connection, sql_query, data=None, batch_size=1000):
    """
    Execute query of the ldvadmin Database
    There is no commit required because we are doing only read queries
    :param: connection: Database 'connection'
    :param: sql_query
    :param: data (optional) the data arguments of the sql_query
    :return result of the query
    """
    try:
        cursor = connection.cursor()
        cursor.execute(sql_query, data)
        connection.commit()
        batch_rows = True
        while batch_rows:
            batch_rows = cursor.fetchmany(batch_size)
            for row in batch_rows:
                yield row

    except Exception as exp:
        logger.error("Could not execute query! '{}' results in -> {}".format(sql_query, exp))


def row_to_dict(dps):
    dps_dict = {"obs_id": dps[0], "oid_source": dps[1], "dataproduct_source": "LOFAR LTA",
                "dataproduct_type": dps[2], "project": dps[3], "activity": dps[4], "surl": dps[5],
                "filesize": dps[6],
                "additional_meta": {"dysco_compression": dps[7], "antenna_set": dps[8], "instrument_filter": dps[9]},
                "location": dps[5]}

    return dps_dict


def query_and_insert(connection, ldvspec_interface,
                     number_of_dataproducts,
                     query, no_limit_to_insert, insert_batch_size):
    results_generator = execute_query(connection, query)

    if no_limit_to_insert:
        lst_all_dps = [row_to_dict(row) for row in results_generator]
        res_lst_ids = ldvspec_interface.insert_multiple_dataproduct(payload=lst_all_dps)
        logging.info("Added %s DataProduct objects", len(res_lst_ids))
        logging.debug("Added with ids=%s", res_lst_ids)
        return len(lst_all_dps)
    else:
        nbr_required_inserts = math.ceil(number_of_dataproducts / insert_batch_size)
        total_count = 0
        for cnt in range(nbr_required_inserts):
            start = cnt * insert_batch_size
            end = start + insert_batch_size

            lst_dps = []
            for _ in range(insert_batch_size):
                row = next(results_generator, None)
                if row:
                    lst_dps.append(row_to_dict(row))
                else:
                    break
            total_count += len(lst_dps)
            res_lst_ids = ldvspec_interface.insert_multiple_dataproduct(payload=lst_dps)
            logging.info("Insert count %s of %s: Added %s DataProduct objects  [%s till %s]", cnt, nbr_required_inserts,
                         insert_batch_size, start, end)
            logging.debug("Added with ids=%s", res_lst_ids)

        return total_count


def main():
    """
    Migrates data from the ldvadmin database to a ldv-spec-db database.
    """
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=logging.INFO)

    # Check the invocation arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("--version", default=False, help="Show current version of this program", action="store_true")
    parser.add_argument("-v", "--verbose", default=False, help="More information at run time.", action="store_true")
    parser.add_argument("-l", "--limit", default=0, type=int, help="Limit on the number of queries (0 is no limit)",
                        action="store")

    parser.add_argument("--host", nargs="?", default='dev',
                        help="The ldv-spec-db host. Presets are 'dev' (default), 'test', 'prod', otherwise give a full url like https://sdc.astron.nl:5554/ldvspec/api/v1")
    parser.add_argument("--configuration", default='~/shared/ldv_migrate.cfg',
                        help="Configuration file containing tunnel and ldvadmin database credentials")
    parser.add_argument("-s", "--section", default='postgresql-ldv',
                        help="Add the configuration's section from the database.cfg.")
    # Have payload of more millions will most likely not work
    # tested with 10.000 results in 90 seconds so # 11 mil. will be at least 28 hours
    parser.add_argument("-r", "--max_nbr_dps_to_insert_per_request", default=1000, type=int,
                        help="The number of dataproducts to insert per REST request (0 is no limit)", action="store")
    parser.add_argument("--token", default="ca1a247b2d9ccb556f450e541874e714e6d04eba", help="Token for ldvspec")

    args = parser.parse_args()

    if args.version:
        # Get file's Last modification time stamp only in terms of seconds since epoch and convert in timestamp
        modification_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(os.path.getmtime(__file__)))
        logging.info("--- {} (last updated {}) ---".format(os.path.basename(__file__), modification_time))
        return

    print("\nMoment please....\n")

    if args.verbose:
        change_logging_level(logging.DEBUG)

    if args.limit == 0:
        limit_str = ""
    else:
        limit_str = "limit {}".format(args.limit)
        logging.debug("Limit on number of dataproducts to query is set to {}".format(args.limit))

    if args.max_nbr_dps_to_insert_per_request == 0:
        no_limit_to_insert = True
    else:
        no_limit_to_insert = False
        logging.debug("Limit on number of dataproducts to insert REST is set to {}".format(
            args.max_nbr_dps_to_insert_per_request))

    # Create connection with ldv-spec-db using REST API, use temp. token created in my test-env
    ldvspec_interface = LDVSpecInterface(args.host, args.token)

    query_count_all_raw_dataproducts = "select count(*) from astrowise.raw_dataproducts"
    query_count_all_pipeline_dataproducts = "select count(*) from astrowise.pl_dataproducts"
    query_all_required_fields_raw_dataproducts = \
        "select obsid, obsid_source, dp_type, project, activity, uri, size, dysco, antenna_set, instrument_filter from astrowise.raw_dataproducts order by id {}" \
            .format(limit_str)

    # Create connection using ssh tunnel with the ldvadmin database
    conn, tunnel = connector.connect_postgresql(args.configuration, args.section)
    count_raw_dps = next(execute_query(conn, query_count_all_raw_dataproducts))[0]
    count_pl_dps = next(execute_query(conn, query_count_all_pipeline_dataproducts))[0]
    logging.info(
        "There are %s raw dataproducts and %s pipeline dataproduct in the ldvadmin.astrowise table!!",
        count_raw_dps, count_pl_dps
    )

    n_inserted_items = query_and_insert(conn, ldvspec_interface, count_raw_dps,
                                        query_all_required_fields_raw_dataproducts,
                                        no_limit_to_insert, args.max_nbr_dps_to_insert_per_request)

    # Are there still dataproducts left to query?
    nbr_dps_left = args.limit - n_inserted_items

    if nbr_dps_left > 0 and args.limit > 0:
        logging.debug("Limit on number of leftover dataproducts to query is set to {}".format(nbr_dps_left))
        limit_str = "limit {}".format(nbr_dps_left)
    else:
        limit_str = ""
    query_all_required_fields_pipeline_dataproducts = \
        "select obsid, obsid_source, dp_type, project, activity, uri, size, dysco from astrowise.pl_dataproducts order by id {}" \
            .format(limit_str)
    n_inserted_items += query_and_insert(conn, ldvspec_interface, count_pl_dps,
                                         query_all_required_fields_pipeline_dataproducts,
                                         no_limit_to_insert, args.max_nbr_dps_to_insert_per_request)

    logging.info("%s dataproduct retrieved from ldvadmin", n_inserted_items)

    print("\nThat's All Folks!\n")


if __name__ == "__main__":
    main()
