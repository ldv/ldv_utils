from configparser import ConfigParser
import os


def read_config(configuration_file, section):
    full_filename = os.path.expanduser(configuration_file)
    parser = ConfigParser()
    read_result = parser.read(full_filename)
    # If file not found then parser returns just an empty list it does not raise an Exception!
    if len(read_result) == 0:
        raise Exception("Configuration file with filename {0} not found".format(full_filename))

    db_settings = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            db_settings[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, full_filename))

    return db_settings
