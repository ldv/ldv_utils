import sys

import psycopg2
import logging
import argparse
from sshtunnel import SSHTunnelForwarder
import os

from ldv_migrate.connection.config import read_config

def connect_postgresql(configuration_file, section):
    """ Connect to the PostgreSQL database server """
    conn = None
    tunnel = None
    try:
        # read connection parameters
        configuration = read_config(configuration_file=configuration_file, section=section)

        logging.info('Connecting PostgreSQL database %s', configuration.get('database', 'no database name given'))

        host = configuration.get('host', 'no host given')
        if host != 'localhost':
            tunnel = open_tunnel(configuration)
            conn = psycopg2.connect(host='localhost',
                                    port=tunnel.local_bind_port,
                                    database=configuration.get('database'),
                                    user=configuration.get('user'),
                                    password=configuration.get('password'))
        else:
            conn = psycopg2.connect(**configuration)

        cur = conn.cursor()
        cur.execute('SELECT version()')
        db_version = cur.fetchone()
        logging.info('Database version: ' + db_version[0])

    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)
        if tunnel is not None:
            tunnel.stop()

    return conn, tunnel


def open_tunnel(configuration_params):
    tunnel_host = configuration_params.get('tunnelhost', "no tunnel host given")
    tunnel_username = configuration_params.get('tunnelusername', "no username for the tunnel given")
    host = configuration_params.get('host', "no host given")
    port = int(configuration_params.get('port', "no port given"))

    # check if a private key and password was given
    ssh_pkey = configuration_params.get('ssh_pkey',None)
    ssh_private_key_password = configuration_params.get('ssh_private_key_password',None)

    logging.info("Creating ssh tunnel for %s and port %s with tunnel host %s and username %s", repr(host), port,
                 repr(tunnel_host), repr(tunnel_username))

    if ssh_pkey:
        ssh_tunnel = SSHTunnelForwarder(
            ssh_address_or_host=tunnel_host,
            ssh_username=tunnel_username,
            remote_bind_address=(host, port),
            ssh_pkey = ssh_pkey,
            ssh_private_key_password = ssh_private_key_password
        )
    else:
        try:
            ssh_config_file = os.path.expanduser("~/.ssh/config")
        except FileNotFoundError as exc:
            raise FileNotFoundError(
                "Ssh config file not found on standard path '~/.ssh/config'. This is mandatory for opening the ssh tunnel"
            ) from exc

        ssh_tunnel = SSHTunnelForwarder(
            ssh_address_or_host=tunnel_host,
            ssh_username=tunnel_username,
            ssh_config_file=ssh_config_file,
            remote_bind_address=(host, port),
        )

    ssh_tunnel.start()
    return ssh_tunnel


def main():
    """
    Opens a database connection from configuration file database.cfg
    """
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=logging.DEBUG)

    # Check the invocation arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--section", help="Add the configuration's section from the database.cfg.")
    args = parser.parse_args()

    if not args.section:
        logging.critical("Error: no configuration section given. Try --help")
        sys.exit(-1)

    return connect_postgresql(args.section)


if __name__ == '__main__':
    connection, server = main()
    if connection is not None:
        connection.close()
        logging.info('Database connection closed.')
    if server is not None:
        server.stop()
        logging.info('Tunneled server stopped.')
