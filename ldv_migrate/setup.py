from setuptools import setup, find_packages

def get_requirements() -> str:
    with open("requirements.txt") as reqs:
        return reqs.read().split("\n")

setup(name='ldvspec-migration',
      version='1.0.0',
      description='Migration script to copy data from ldvadmin to ldv-spec-db',
      url='https://git.astron.nl/astron-sdc/ldv-specification/-/tree/main/ldvspec/scripts',
      author='Roy de Goei, Fanna Lautenback, Nico Vermaas',
      author_email='vermaas@astron.nl',
      license='Apache 2.0',
      install_requires=get_requirements(),
      packages=find_packages(),
      py_modules = ['ldv_migrate','ldv_migrate.connection'],
      include_package_data=True,
      entry_points={
            'console_scripts': [
                  'ldv_migrate=ldv_migrate.migrate_ldvadmin_to_ldvspec:main',
            ],
      },
      )