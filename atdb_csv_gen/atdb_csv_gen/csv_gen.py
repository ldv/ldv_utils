#!/usr/bin/env python3
# SPDX-License-Identifier: Apache-2.0

import logging
import os
from argparse import ArgumentParser, Namespace
from sys import version
from typing import Iterator, List, Optional, Tuple

from dotenv import load_dotenv
from sqlalchemy import create_engine as sqla_create_engine
from sqlalchemy.engine.base import Connection, Engine
from sqlalchemy.sql import text

logger = logging.getLogger(__name__)
_EXPECTED_STR_PER_DATATYPE = {
    'MS': '.MS_',
    'BF': '_bf.'
}


def parse_args() -> Namespace:
    """Parse CLI arguments"""

    parser = ArgumentParser(
        description="Generates CSV for atdb_load_tasks_from_table\n"
                    "Requires the following env vars:\n"
                    "\tDB_USERNAME, DB_PASSWORD, DB_DATABASE and optionally:\n"
                    "\tDB_HOST (localhost by default) and DB_PORT (5432, postgres default)"
    )
    parser.add_argument("obs_id", type=str, help="Observation ID (SAS ID)")
    parser.add_argument("-o", type=str, help="Output file")
    parser.add_argument(
        "-c", type=str, help="Config file (in .env format)", default=None
    )
    parser.add_argument(
        "--save-missing", type=str, help="Directory to store csv with missing files"
    )
    parser.add_argument("--datatype", help="Select a specific datatype", choices=_EXPECTED_STR_PER_DATATYPE.keys())
    parser.add_argument("-v", action="store_true", help="Verbose logging")
    parser.add_argument("-q", action="store_true", help="Quiet logging (only errors)")
    return parser.parse_args()


def fetch_registered_files(conn: Connection, obs_id: str) -> List[Tuple[int, str]]:
    """Fetch the registered files from the LTA"""

    sql = text("SELECT file_size, surl FROM ldv_delete.aw_uris WHERE obsid = :obs_id")
    return [row for row in conn.execute(sql, obs_id=obs_id)]


def fetch_crawled_files(conn: Connection, obs_id: str) -> List[Tuple[int, str]]:
    """Fetch crawled files from the LTA"""

    sql = text(
        "SELECT file_size, surl FROM ldv_delete.lta_uris WHERE dir_name like CONCAT('/pnfs/grid.sara.nl/data/lofar/ops/projects/%/',:obs_id)"
    )
    return [row for row in conn.execute(sql, obs_id=obs_id)]


def tuple_list_to_csv(l: List[Tuple[int, str]], sep: str = ";") -> Iterator[str]:
    """Convert a list of tuples to a line in a csv"""

    return map(lambda row: f"{row[0]}{sep}{row[1]}", l)


def write_output(csv_data: str, output: Optional[str]):
    """Write csv data to output file or stdout"""
    if output is not None:
        dirs = os.path.dirname(output)
        if dirs != "":
            os.makedirs(dirs, exist_ok=True)

        with open(output, "w") as o_file:
            o_file.write(csv_data)

        logger.debug("Output has been written to %s", output)
    else:
        print(csv_data)


def create_engine(env_file: Optional[str] = None) -> Engine:
    """Create sqlalchemy engine from env vars or config file"""

    # Optionally load the config file
    if env_file is not None:
        load_dotenv(env_file)

    USER = os.getenv("DB_USERNAME")
    PASSWORD = os.getenv("DB_PASSWORD")
    DATABASE = os.getenv("DB_DATABASE")
    HOST = os.getenv("DB_HOST", "localhost")
    PORT = os.getenv("DB_PORT", "5432")

    if not all((USER, PASSWORD, DATABASE, HOST, PORT)):
        raise RuntimeError(
            "Missing DB credentials in env vars.\nDid you export the .env file?"
        )

    engine = sqla_create_engine(f"postgresql://{USER}:{PASSWORD}@{HOST}:{PORT}/{DATABASE}")
    logger.debug("Connected to %s:%s", HOST, PORT)

    return engine


def query_file_lists(
        obs_id: str, config_file: Optional[str] = None
) -> Tuple[List[Tuple[int, str]], List[Tuple[int, str]]]:
    """Query database for both file lists"""

    engine = create_engine(config_file)

    logger.info("Query file lists...")
    with engine.connect() as conn:
        conn: Connection
        registered = fetch_registered_files(conn, obs_id)
        logger.debug("Found %i registered files", len(registered))
        crawled = fetch_crawled_files(conn, obs_id)
        logger.debug("Found %i crawled files", len(crawled))
    logger.info("Query complete")
    return registered, crawled


def match_file_lists(
        registered: List[Tuple[int, str]],
        crawled: List[Tuple[int, str]],
        save_missing: Optional[str] = None,
) -> List[Tuple[int, str]]:
    # Create set for easier manipulation
    reg_data = set(tuple_list_to_csv(registered))
    cra_data = set(tuple_list_to_csv(crawled))

    # Registered, no file found
    reg_no_file = reg_data.difference(cra_data)
    if len(reg_no_file) > 0:
        logger.warning(
            "Observation contains %i registered files which are not present on disk",
            len(reg_no_file),
        )
        for row in reg_no_file:
            logger.warning(row)
        if save_missing:
            write_output(
                "\n".join(reg_no_file), os.path.join(save_missing, "reg_no_file.csv")
            )

    # File found, not registered
    file_no_reg = cra_data.difference(reg_data)
    if len(file_no_reg) > 0:
        logger.warning(
            "Observation contains %i files on disk which are not registered in the LTA",
            len(file_no_reg),
        )
        for row in file_no_reg:
            logger.warning(row)
        if save_missing:
            write_output(
                "\n".join(file_no_reg), os.path.join(save_missing, "file_no_reg.csv")
            )

    return reg_data.intersection(cra_data)


def filter_datatype(size_path_list, datatype):
    if datatype is None:
        return size_path_list
    else:
        expected_string = _EXPECTED_STR_PER_DATATYPE[datatype]
        return [item for item in size_path_list if expected_string in item]


def gen_csv(
        obs_id: str,
        o_file: Optional[str] = None,
        save_missing: Optional[str] = None,
        config_file: Optional[str] = None,
        datatype: Optional[str] = None,
):
    """Generate CSV file for ATDB

    Parameters
    ----------
    obs_id : string
        observation id
    o_file : string, optional
        File to write the output to (by default to stdout)
    save_missing : string, optional
        Directory to write missing files output to (by default only shows warnings!)
    config_file: string, optional
        File in .env format to load configuration from
    """

    registered, crawled = query_file_lists(obs_id, config_file)

    intersection = match_file_lists(registered, crawled, save_missing)

    filtered_intersection = filter_datatype(intersection, datatype)

    write_output("\n".join(filtered_intersection), o_file)


def main():
    args = parse_args()

    if args.v:
        logging.basicConfig(level=logging.DEBUG)
    elif args.q:
        logging.basicConfig(level=logging.ERROR)
    else:
        logging.basicConfig(level=logging.INFO)

    gen_csv(
        obs_id=args.obs_id,
        o_file=args.o,
        save_missing=args.save_missing,
        config_file=args.c,
        datatype=args.datatype
    )


if __name__ == "__main__":
    main()
