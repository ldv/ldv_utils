# SPDX-License-Identifier: Apache-2.0

from setuptools import setup

def get_requirements() -> str:
    with open("requirements.txt") as reqs:
        return reqs.read().split("\n")

setup(
    name="atdb_csv_gen",
    version="1.0.0",
    description="Migration script to copy data from ldvadmin to ldv-spec-db",
    author='Roy de Goei, Fanna Lautenbach, Nico Vermaas for ASTRON',
    packages=["atdb_csv_gen"],
    install_requires=get_requirements(),
    scripts=["bin/atdb_csv_gen"],
    license="Apache License, Version 2.0",
    zip=False,
    python_requires=">=3.6.8"
)
