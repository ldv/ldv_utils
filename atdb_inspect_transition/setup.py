# SPDX-License-Identifier: Apache-2.0

from setuptools import setup


def get_requirements() -> str:
    with open("requirements.txt") as reqs:
        return reqs.read().split("\n")

setup(
    name="atdb-inspect-transition",
    version="1.0.0",
    description="Tool to gather ATDB transition time",
    author="ASTRON",
    packages=["atdb_inspect_transition"],
    install_requires=get_requirements(),
    scripts=["bin/atdb_inspect_transition"],
    license="Apache License, Version 2.0",
    zip=False,
    python_requires=">=3.6.8"
)
