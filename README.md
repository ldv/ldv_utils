# LDV Utility scripts

## ATDB CSV Gen
Generates csv input files for the `atdb_load_tasks_from_table` script.

### Installation
```bash
# Using pip install (requires valid ssh key, until the repo is set to public)
pip install pip install -e "git+https://git.astron.nl/ldv/ldv_utils.git#egg=atdb-csv-gen&subdirectory=atdb_csv_gen"
```

### Running
```bash
# copy .env.example to .env and fill in the environment vars (DB credentials)
cp ./atdb_csv_gen/.env.example ./atdb_csv_gen/.env

# run using python
export $(cat .env | xargs)  # or export env vars elsewhere
atdb_csv_gen -v -o out.csv 650273

# or using a config file following .env.example layout:
atdb_csv_gen -c path/to/config.env -o out.csv 650273

# More info and flags
atdb_csv_gen -h
```

### Caveats
Only warns when files are encountered in either list, but not in both. Only creates a list of files which are present in both queries.
### WARNING
To execute this script you have to be able to connect to sdc-db.astron.nl. 
This is not possible from outside the ASTRON network and without the proper credentials.
Please contact the sys admin or the SDC architect.


## ATDB Result Gatherer
Gather from the ATDB database the values of tasks.

### Installation
```bash
# Using pip install (requires valid ssh key, until the repo is set to public)
pip install pip install -e "git+https://git.astron.nl/ldv/ldv_utils.git#egg=atdb-result-gather&subdirectory=atdb_result_gatherer"
```

### Running
```bash
# run using python
atdb_result_gatherer [sas_id] [path] [path]

# or if you want to inspect the format of a task you can ran
atdb_result_gatherer [sas_id] --print_first

# If you want to specify a different atdb url you can do it with 
atdb_result_gatherer --atdb_url https://youratdbserver/atdb/ [sas_id] [paths]
```
## ATDB Inspect Transition
Inspect the time elapsed between the transition of two states given a sas id.

### Installation
```bash
# Using pip install (requires valid ssh key, until the repo is set to public)
pip install pip install -e "git+https://git.astron.nl/ldv/ldv_utils.git#egg=atdb-inspect-transition&subdirectory=atdb_inspect_transition"
```

### Running
```bash
atdb_inspect_transition [sas_id] [from status] [to status]

# More info and flags
atdb_inspect_transition -h
```


## ldv-migrate script
This is a script to migrate the data from the old `ldvadmin` database to the new `ldv-spec-db` database.

### Deploying (manually)
For example on `sdc@dop814.astron.nl` (this is our sdc-dev test machine) 

```
# only once
> cd ~
> mkdir ldv-migrate
> cd ldvspec-migrate
> virtualenv env3.9 -p python3.9 (on ubuntu)
> python3 -m venv env3.9 (on centos)
> source env3.9/bin/activate
> pip3 install --upgrade setuptools pip
```

### Installation
```bash
# Using pip install (requires valid ssh key, until the repo is set to public)

pip install -e "git+https://git.astron.nl/ldv/ldv_utils.git#egg=ldvspec-migration&subdirectory=ldv_migrate" --upgrade

```

### Configuration
The database and tunnel configuration are in a local file on the host that can be given as a `--configuration` parameter.
The parameter file can contain a link to a private key file, and password. 
When those keys are not given, the script will try to read the local SSH_CONFIG file `~/.ssh/config`. (Note that this does not work on Windows)

See for more documentation about the sshtunnel mechanism:
https://pypi.org/project/sshtunnel/ 

The following example shows a local configuration using private key.
```
[postgresql-local]
host=localhost
port=5433
database=ldv-spec-db
user=postgres
password=xxxxx

[postgresql-ldv]
tunnelhost=dop821.astron.nl
tunnelusername=sdco
host=sdc-db.astron.nl
port=5432
database=ldvadmin
user=ldvrbow
password=xxxxx
ssh_pkey = "C:\\Program Files Nico\\putty\\astron_private_key.ppk"
ssh_private_key_password = "xxxxx"
```

### Running
To test if it works
```bash
> ldv_migrate -h

usage: ldv_migrate [-h] [--version] [-v] [-l LIMIT] [-t TOKEN] [--host [HOST]] [-s SECTION] [-r MAX_NBR_DPS_TO_INSERT_PER_REQUEST]

optional arguments:
  -h, --help            show this help message and exit
  --version             Show current version of this program
  -v, --verbose         More information at run time.
  -l LIMIT, --limit LIMIT
                        Limit on the number of queries (0 is no limit)
  -t TOKEN, --token TOKEN
                        Token to access the REST API of ldvspec
  --host [HOST]         The ldv-spec-db host.
  --configuration CONFIGURATION
                        Configuration file containing tunnel and ldvadmin database credentials
  -s SECTION, --section SECTION
                        Add the configuration's section from the database.cfg.
  -r MAX_NBR_DPS_TO_INSERT_PER_REQUEST, --max_nbr_dps_to_insert_per_request MAX_NBR_DPS_TO_INSERT_PER_REQUEST
                        The number of dataproducts to insert per REST request (0 is no limit)

```

### Examples
Some examples:
```
- Show latest version:
     ldv_migrate --version
- Import only 1000 records and show more verbosity:
     ldv_migrate --limit 1000 --verbose
- Import 50000 records and insert in steps of 10000 (so 5 steps)
     ldv_migrate --limit 50000 --max_nbr_dps_to_insert_per_request 10000
- Import only 1000 records at production:
     ldv_migrate --limit 1000 --host prod

- Import 10000 records into the ldv-spec-db:12000 test database on sdc-dev
  ldv_migrate --limit 10000 --verbose --configuration ~/shared/ldv_migrate_sdc-dev.cfg --token 2cd0e124abf17e803e2ce0a664225b5e3dbaeaa6 --host test
```
