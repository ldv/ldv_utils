# SPDX-License-Identifier: Apache-2.0

from setuptools import setup


def get_requirements() -> str:
    with open("requirements.txt") as reqs:
        return reqs.read().split("\n")

setup(
    name="atdb-result-gather",
    version="1.0.0",
    description="Tool to gather ATDB tasks information",
    author="ASTRON",
    packages=["atdb_result_gatherer"],
    install_requires=get_requirements(),
    scripts=["bin/atdb_result_gatherer"],
    license="Apache License, Version 2.0",
    zip=False,
    python_requires=">=3.6.8"
)
